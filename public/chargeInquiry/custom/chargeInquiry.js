    	$(document).ready(function(){

            const monthNames = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN",
              "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
            ];

            var cntr = $('#cntr').val();
            var pickup = $('#pickup').val();


           if(cntr != ''){
                 var date_input = new Date($('#pickup').val());


            loading = '<tr><td colspan="6" align="center"><img src="public/img/load-charge.gif" class="img-responsive" width="25%"></td></tr>';
                

             $("#ChargeInquiry").append(loading);



            if(cntr == ''){

                $(".warn-cntr").html('Please Input ContainerNumber.');

            } else if (pickup == ''){

                $(".warn-pickup").html('Please Input PickUp Date.');

            } else {
                $.ajax({
             url: 'Sandbox.asmx/ChargeInquiry',
             method: 'POST',
             dataType: 'xml',
             data : { cntr:cntr }
             }).done(function (data) {


                console.log('Connecting service Charge Inquiry.');


                console.log('Search Charge Inquiry Complete.');

                var ChargeInquiry = '';
                var warn = '';

                $(data).find('ChargeInquiry').each(function(){


                    var date_input1 = new Date($(this).find('COMPLETE_DISCHARGE_TM').text());

                    $('#cod').val($(this).find('COMPLETE_DISCHARGE_TM').text());


                    var size =  $(this).find('EQPT_LENGTH').text();
                    var load =  $(this).find('CNTR_LOAD_STATUS').text();

                    if(size == '20' && load == 'F'){
                        Type = 'LN201';
                        Relo = 'RL201';
                        ds = '20';
                    } else if (size == '40' && load == 'F'){
                        Type = 'LN401';
                        ds = ' 40';
                        Relo = 'RL401';
                    } else if (size == '45' && load == 'F'){
                        Type = 'LN451';
                        ds = '45';
                        Relo = 'RL451';
                    } else if (size == '20' && load == 'E'){
                        Type = 'LN202';
                        ds = '20';
                        Relo = 'RL201';
                    } else if (size == '40' && load == 'E'){
                        Type = 'LN402';
                        ds = '40';
                        Relo = 'RL401';
                    } else if (size == '45' && load == 'E'){
                        Type = 'LN452';
                        ds = '45';
                        Relo = 'RL451';
                    }

                    //liftON


                    var current = new Date();

                    var today = new Date(date_input);
                    var dd = today.getDate();
                    var mm =  monthNames[today.getMonth()]; //January is 0!
                    var yyyy = today.getFullYear();


                    var validateDate = current.setDate(current.getDate() -1);

                    var checkDate = new Date(validateDate);



                    var container = $(this).find('CNTR_NUM').text();


                    ChargeInquiry += '<tr>';
                    ChargeInquiry += '<td style="text-align: left;">';
                    ChargeInquiry += 'LIFT ON Size ' + ds + "'";
                    ChargeInquiry += '</td>';

                    ChargeInquiry += '<td style="text-align: left;">';
                    ChargeInquiry += dd + '-' + mm + '-' + yyyy+' / '+ dd + '-' + mm + '-' + yyyy;
                    ChargeInquiry += '</td>';

                    ChargeInquiry += '<td style="text-align: center;">';
                    ChargeInquiry += 1;
                    ChargeInquiry += '</td>';

                    ChargeInquiry += '<td style="text-align: center;">';
                    ChargeInquiry += 1;
                    ChargeInquiry += '</td>';

                    ChargeInquiry += '<td style="text-align: right;">';
                    ChargeInquiry += '<p id="rate1"></p>';
                    ChargeInquiry += '</td>';

                    ChargeInquiry += '<td style="text-align: right;">';
                    ChargeInquiry += '<p id="rate7"></p>';
                    ChargeInquiry += '</td>';

                    ChargeInquiry += '</tr>';

                    //storage

                    var today_sto1 = new Date(date_input1);

                    var storage1A = today_sto1.setDate(today_sto1.getDate() + 3);

                    var today_sto2 = new Date(storage1A);
                    var storage1B = today_sto1.setDate(today_sto2.getDate() + 6);

                    var modifySto1a = new Date(storage1A);

                    var dd_sto1a = modifySto1a.getDate();

                    var mm_sto1 =  monthNames[modifySto1a.getMonth()]; //January is 0!

                    var yyyy_sto1 = modifySto1a.getFullYear();

                    //////////////////////////////////////////////////////

                    var modifySto1b = new Date(storage1B);

                    var dd_sto1b = modifySto1b.getDate();

                    var mm_sto1b =  monthNames[modifySto1b.getMonth()]; //January is 0!

                    var yyyy_sto1b = modifySto1b.getFullYear();

                    //////////////////////////////////////////////////////
                    //DiffDateNormal

                    var timeDiffN = Math.abs(modifySto1b.getTime() - modifySto1a.getTime());
                    var diffDaysNSto1 = Math.ceil(timeDiffN / (1000 * 3600 * 24) + 1 ); 


                    //////////////////////////////////////////////////////
                    //DiffDateLogic

                    var timeDiffL = Math.abs(modifySto1a.getTime() - today.getTime());
                    var diffDaysLSto1 = Math.ceil(timeDiffL / (1000 * 3600 * 24) + 1 ); 




                    if(date_input >= modifySto1a && date_input > modifySto1b){

                        ChargeInquiry += '<tr>';
                        ChargeInquiry += '<td style="text-align: left;">';
                        ChargeInquiry += "Storage1 " + ds + "'";
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: left;">';
                        ChargeInquiry += dd_sto1a + '-' + mm_sto1 + '-' + yyyy_sto1 + ' / ' + dd_sto1b + '-' + mm_sto1b + '-' + yyyy_sto1b;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: center;">';
                        ChargeInquiry += 1;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: center;">';
                        ChargeInquiry += diffDaysNSto1;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: right;">';
                        ChargeInquiry += '<p id="rate2"></p>';
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: right;">';
                        ChargeInquiry += '<p id="cal-rate2"></p>';
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '</tr>';

                        $('#date2').val(diffDaysNSto1);

                    } else if (date_input >= modifySto1a && date_input <= modifySto1b){

                        ChargeInquiry += '<tr>';
                        ChargeInquiry += '<td style="text-align: left;">';
                        ChargeInquiry += "Storage1 " + ds + "'";
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: left;">';
                        ChargeInquiry += dd_sto1a + '-' + mm_sto1 + '-' + yyyy_sto1 + ' / '+ dd + '-' + mm + '-' + yyyy+1;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: center;">';
                        ChargeInquiry += 1;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: center;">';
                        ChargeInquiry += diffDaysLSto1;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: right;">';
                        ChargeInquiry += '<p id="rate2"></p>';
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: right;">';
                        ChargeInquiry += '<p id="cal-rate2"></p>';
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '</tr>';

                        $('#date2').val(diffDaysLSto1);

                    } 

                    

                    //storage


                    var today_sto2 = new Date(storage1B);

                    var modifySto2a = today_sto2.setDate(today_sto2.getDate() + 1);

                    var modifySto2b = today_sto2.setDate(today_sto2.getDate() + 6);

                    var modifySto2 = new Date(modifySto2a);

                    var dd_sto2a = modifySto2.getDate();

                    var mm_sto2a =  monthNames[modifySto2.getMonth()]; //January is 0!

                    var yyyy_sto2a = modifySto2.getFullYear();


                    var modifyStor2 = new Date(modifySto2b);

                    var dd_sto2b = modifyStor2.getDate();

                    var mm_sto2b =  monthNames[modifyStor2.getMonth()]; //January is 0!

                    var yyyy_sto2b = modifyStor2.getFullYear();

                    //////////////////////////////////////////////////////
                    //DiffDateNormal

                    var timeDiffN2 = Math.abs(modifySto2.getTime() - modifyStor2.getTime());
                    var diffDaysNSto2 = Math.ceil(timeDiffN2 / (1000 * 3600 * 24) + 1 ); 


                    //////////////////////////////////////////////////////
                    //DiffDateLogic

                    var timeDiffL2 = Math.abs(modifyStor2.getTime() - today.getTime());
                    var diffDaysLSto2 = Math.ceil(timeDiffL2 / (1000 * 3600 * 24) + 1 ); 
                   

                    if(date_input >= modifySto2 && date_input > modifyStor2){

                        ChargeInquiry += '<tr>';
                        ChargeInquiry += '<td style="text-align: left;">';
                        ChargeInquiry += 'Storage2 ' + ds + "'";
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: left;">';
                        ChargeInquiry += dd_sto2a + '-' + mm_sto2a + '-' + yyyy_sto2a + ' / ' + dd_sto2b + '-' + mm_sto2b + '-' + yyyy_sto2b;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: center;">';
                        ChargeInquiry += 1;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: center;">';
                        ChargeInquiry += diffDaysNSto2;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: right;">';
                        ChargeInquiry += '<p id="rate3"></p>';
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: right;">';
                        ChargeInquiry +=  '<p id="cal-rate3"></p>';
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '</tr>';

                        $('#date3').val(diffDaysNSto2);

                    } else if(date_input >= modifySto2 && date_input <= modifyStor2){

                        ChargeInquiry += '<tr>';
                        ChargeInquiry += '<td style="text-align: left;">';
                        ChargeInquiry += 'Storage2 ' + ds + "'";
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: left;">';
                        ChargeInquiry += dd_sto2a + '-' + mm_sto2a + '-' + yyyy_sto2a + ' / ' + dd + '-' + mm + '-' + yyyy;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: center;">';
                        ChargeInquiry += 1;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: center;">';
                        ChargeInquiry += diffDaysLSto2;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: right;">';
                        ChargeInquiry += '<p id="rate3"></p>';
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: right;">';
                        ChargeInquiry += '<p id="cal-rate4"></p>';
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '</tr>';

                        $('#date3').val(diffDaysLSto2);

                    }


                    //storage

                    var today_sto3 = new Date(modifySto2b);

                    var modifySto3a = today_sto3.setDate(today_sto3.getDate() + 1);

                    var modifySto3b = today_sto3.setDate(today_sto3.getDate() + 6);

                    var modifySto3 = new Date(modifySto3a);

                    var dd_sto3a = modifySto3.getDate();

                    var mm_sto3a =  monthNames[modifySto3.getMonth()]; //January is 0!

                    var yyyy_sto3a = modifySto3.getFullYear();

                    var modifyStor3 = new Date(modifySto3b);

                    var dd_sto3b = modifyStor3.getDate();

                    var mm_sto3b =  monthNames[modifyStor3.getMonth()]; //January is 0!

                    var yyyy_sto3b = modifyStor3.getFullYear();

                    //////////////////////////////////////////////////////
                    //DiffDateNormal

                    var timeDiffN3 = Math.abs(modifySto3.getTime() - modifyStor3.getTime());
                    var diffDaysNSto3 = Math.ceil(timeDiffN3 / (1000 * 3600 * 24) + 1 ); 


                    //////////////////////////////////////////////////////
                    //DiffDateLogic

                    var timeDiffL3 = Math.abs(modifySto3.getTime() - today.getTime());
                    var diffDaysLSto3 = Math.ceil(timeDiffL3 / (1000 * 3600 * 24) + 1 ); 
                   

                    if (date_input >= modifySto3 && date_input > modifyStor3) {

                        ChargeInquiry += '<tr>';
                        ChargeInquiry += '<td style="text-align: left;">';
                        ChargeInquiry += 'Storage3 ' + ds + "'";
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: left;">';
                        ChargeInquiry += dd_sto3a + '-' + mm_sto3a + '-' + yyyy_sto3a + ' / '  + dd + '-' + mm + '-' + yyyy;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: center;">';
                        ChargeInquiry += 1;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: center;">';
                        ChargeInquiry += diffDaysLSto3;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: right;">';
                        ChargeInquiry += '<p id="rate4"></p>';
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: right;">';
                        ChargeInquiry += '<p id="cal-rate4"></p>';
                        ChargeInquiry += '</td>';

                        $('#date4').val(diffDaysLSto3);
 

                    }  else if (date_input >= modifySto3 && date_input <= modifyStor3) {

                        ChargeInquiry += '<tr>';
                        ChargeInquiry += '<td style="text-align: left;">';
                        ChargeInquiry += 'Storage3 ' + ds + "'";
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: left;">';
                        ChargeInquiry += dd_sto3a + '-' + mm_sto3a + '-' + yyyy_sto3a + ' / '  + dd + '-' + mm + '-' + yyyy;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: center;">';
                        ChargeInquiry += 1;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: center;">';
                        ChargeInquiry += diffDaysLSto3;
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: right;">';
                        ChargeInquiry += '<p id="rate4"></p>';
                        ChargeInquiry += '</td>';

                        ChargeInquiry += '<td style="text-align: right;">';
                        ChargeInquiry += '<p id="cal-rate4"></p>';
                        ChargeInquiry += '</td>';

                        $('#date4').val(diffDaysLSto3);
 

                    }

                    


                    //relocation

                    var relo_date = new Date(date_input1);

                    var re_location = relo_date.setDate(relo_date.getDate() + 8);

                    var modifyReLo = new Date(re_location);

                    var dd_relocation = modifyReLo.getDate();

                    var mm_relocation =  monthNames[modifyReLo.getMonth()]; //January is 0!

                    var yyyy_relocation = modifyReLo.getFullYear();

                    ChargeInquiry += '<tr>';
                    ChargeInquiry += '<td style="text-align: left;">';
                    ChargeInquiry += 'RELOCATION ' + ds + "'";
                    ChargeInquiry += '</td>';

                    ChargeInquiry += '<td style="text-align: left;">';
                    ChargeInquiry += dd_relocation + '-' + mm_relocation + '-' + yyyy_relocation + ' / ' + dd_relocation + '-' + mm_relocation + '-' + yyyy_relocation;
                    ChargeInquiry += '</td>';

                    ChargeInquiry += '<td style="text-align: center;">';
                    ChargeInquiry += 1;
                    ChargeInquiry += '</td>';

                    ChargeInquiry += '<td style="text-align: center;">';
                    ChargeInquiry += 1;
                    ChargeInquiry += '</td>';

                    ChargeInquiry += '<td style="text-align: right;">';
                    ChargeInquiry += '<p id="rate5"></p>';
                    ChargeInquiry += '</td>';

                    ChargeInquiry += '<td style="text-align: right;">';
                    ChargeInquiry += '<p id="rate6"></p>';
                    ChargeInquiry += '</td>';


                    ChargeInquiry += '</tr>';

                     if($(this).find('SPECIAL_OPERATION_CODE').text() == 'DDI' && $(this).find('BIS_EQP_MODE').text() == 'DG'){

                        demo.initChartist();

                        $.notify({
                            icon: 'ti-server',
                            message: "Cannot Process This Container, Please Contact to JWD."

                        },{
                            type: 'warning',
                            timer: 2000,
                            placement: {
                                from: 'top',
                                align: 'center'
                            }
                        });

                        warn += '<tr>';
                        warn += '<td colspan="6" style="text-align:center;">';
                        warn += '<p style="color:red;">This Container is Direct Discharge and "DG" , Please Contact to JWD</p>'
                        warn += '</td>';
                        warn += '</tr>';

                        $("#ChargeInquiry").html('');
                        $("#ChargeInquiry").append(warn);


                    } else if(date_input < date_input1 || date_input < checkDate){

                        demo.initChartist();

                        $.notify({
                            icon: 'ti-server',
                            message: "Wrong Pick Up Date, Please Try Again."

                        },{
                            type: 'warning',
                            timer: 2000,
                            placement: {
                                from: 'top',
                                align: 'center'
                            }
                        });

                        warn += '<tr>';
                        warn += '<td colspan="6" style="text-align:center;">';
                        warn += '<p style="color:red;">Wrong Pick Up Date, Please Try Again.</p>'
                        warn += '</td>';
                        warn += '</tr>';

                        $("#ChargeInquiry").html('');
                        $("#ChargeInquiry").append(warn);


                    } else {

                        $("#ChargeInquiry").html('');
                        $("#ChargeInquiry").append(ChargeInquiry);

                        //lifton
                         $.ajax({
                             url: 'Sandbox.asmx/RateCharge',
                             method: 'POST',
                             dataType: 'xml',
                             data : { Type:Type }
                        }).done(function (data) {

                            $(data).find('RateCharge').each(function(){
                                $('#rate1').append($(this).find('UNIT_RATE').text());
                                $('#rate7').append($(this).find('UNIT_RATE').text());
                            });

                        });

                        //storage1
                         $.ajax({

                             url: 'Sandbox.asmx/RateStorage'+size,
                             method: 'POST',
                             dataType: 'xml',
                             data : { range:1 }
                        }).done(function (data) {


                            $(data).find('RateCharge').each(function( ){


                                $('#rate2').append($(this).find('UNIT_RATE').text());
                                $('#calrate2').val($(this).find('UNIT_RATE').text());

                                var process2 = $('#date2').val()*$(this).find('UNIT_RATE').text();

                                var math2 = parseFloat(Math.round(process2 * 100) / 100).toFixed(2);

                                $('#cal-rate2').append(math2.toLocaleString('en'));
                                
                            });

                        });

                        //storage2
                         $.ajax({

                             url: 'Sandbox.asmx/RateStorage'+size,
                             method: 'POST',
                             dataType: 'xml',
                             data : { range:15 }
                        }).done(function (data) {


                            $(data).find('RateCharge').each(function( ){


                                $('#rate4').append($(this).find('UNIT_RATE').text());
                                $('#calrate4').val($(this).find('UNIT_RATE').text());

                                var process4 = $('#date4').val()*$(this).find('UNIT_RATE').text();

                                var math4 = parseFloat(Math.round(process4 * 100) / 100).toFixed(2);

                                $('#cal-rate4').append(math4.toLocaleString('en'));
                                
                            });

                        });

                        //storage3
                         $.ajax({

                             url: 'Sandbox.asmx/RateStorage'+size,
                             method: 'POST',
                             dataType: 'xml',
                             data : { range:8 }
                        }).done(function (data) {


                            $(data).find('RateCharge').each(function( ){


                                $('#rate3').append($(this).find('UNIT_RATE').text());
                                $('#calrate3').val($(this).find('UNIT_RATE').text());

                                var process3 = $('#date3').val()*$(this).find('UNIT_RATE').text();

                                var math3 = parseFloat(Math.round(process3 * 100) / 100).toFixed(2);

                                $('#cal-rate3').append(math3.toLocaleString('en'));

                                
                            });

                        });

                        //re-location
                         $.ajax({
                             url: 'Sandbox.asmx/RateCharge',
                             method: 'POST',
                             dataType: 'xml',
                             data : { Type:Relo }
                        }).done(function (data) {

                            $(data).find('RateCharge').each(function(){

                                $('#rate5').append($(this).find('UNIT_RATE').text());
                                $('#rate6').append($(this).find('UNIT_RATE').text());
                            });

                        });


                            demo.initChartist();

                            $.notify({
                                icon: 'ti-server',
                                message: "Search Complete."

                            },{
                                type: 'success',
                                timer: 2000,
                                placement: {
                                    from: 'top',
                                    align: 'center'
                                }
                            });

                    }


                });

            }).fail(function (err) {

                    demo.initChartist();

                    $.notify({
                        icon: 'ti-server',
                        message: "Fail to Connect Service Charge Inquiry, Please try again."

                    },{
                        type: 'danger',
                        timer: 2000,
                        placement: {
                            from: 'top',
                            align: 'center'
                        }
                    });

                     $("#ChargeInquiry").html('');

                    console.log('Fail to Connect Service Charge Inquiry, Please try again.');
                    /*setTimeout(function(){
                        window.location.reload(1);
                    }, 4000);*/
            });  

            }



           }

    	});