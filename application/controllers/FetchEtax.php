<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require('Welcome.php');


class FetchEtax extends Welcome {

    public function ReFetchCashOrder(){

            require_once('nusoap/nusoap.php');
                $client = new nusoap_client('http://www.lcit.com/ServiceEtax.asmx?wsdl', 'WSDL');

                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }

                $orderReceipt = $client->call('ReFetchtCashDate','', '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($orderReceipt as $rs) {
                    $jsonData = $rs;
                }

                $de_orderReceipt = json_decode($jsonData, true);

                foreach($de_orderReceipt as $rs_order){

                    $this->db->where('RECEIPT_NO',$rs_order['RECEIPT_NO']);
                    $Order  = $this->db->get('info_cash_etax')->result_array();

                    if(count($Order) == 0){

                        $data = array(  
                            "RECEIPT_NO" => $rs_order['RECEIPT_NO'],
                            "CREATED" => date('Y-m-d H:i:s')
                        );
                                    
                        $this->db->insert('info_cash_etax', $data);

                        echo $rs_order['RECEIPT_NO'].'<br>';

                    }
                    
                }


    }

    public function FetchCashOrder(){

            require_once('nusoap/nusoap.php');
                $client = new nusoap_client('http://www.lcit.com/ServiceEtax.asmx?wsdl', 'WSDL');

                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }

                $orderReceipt = $client->call('FetchtCashDate',array('S_DATE' => date('d-M-Y'), 'E_DATE' => date('d-M-Y')), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($orderReceipt as $rs) {
                    $jsonData = $rs;
                }

                $de_orderReceipt = json_decode($jsonData, true);

                foreach($de_orderReceipt as $rs_order){

                    $this->db->where('RECEIPT_NO',$rs_order['RECEIPT_NO']);
                    $Order  = $this->db->get('info_cash_etax')->result_array();

                    if(count($Order) == 0){

                        $data = array(  
                            "RECEIPT_NO" => $rs_order['RECEIPT_NO'],
                            "CREATED" => date('Y-m-d H:i:s')
                        );
                                    
                        $this->db->insert('info_cash_etax', $data);

                    }
                    
                }


    }

    public function ERCashReceipt() {

        $this->db->order_by('E_ID', 'ASC');
        $this->db->where('IS_SEND','ER');
        $this->db->like('RESPON_ETAX', 'FL007', 'after'); 
        $FL007  = $this->db->get('info_cash_etax')->result_array();

        foreach($FL007 as $rs) {
            $data = array( 
                "IS_SEND" => 'N',
                "IS_FILES" => 'N'
            );
                                
            $this->db->where('RECEIPT_NO', $rs['RECEIPT_NO']);
            $this->db->update('info_cash_etax', $data);
        }

        $this->db->order_by('E_ID', 'ASC');
        $this->db->where('IS_SEND','ER');
        $this->db->like('RESPON_ETAX', 'FL001', 'after'); 
        $FL001  = $this->db->get('info_cash_etax')->result_array();

        foreach($FL001 as $rs) {
            $data = array( 
                "IS_SEND" => 'N',
                "IS_FILES" => 'N'
            );
                                
            $this->db->where('RECEIPT_NO', $rs['RECEIPT_NO']);
            $this->db->update('info_cash_etax', $data);
        }


        $this->db->order_by('E_ID', 'ASC');
        $this->db->where('IS_SEND','ER');
        $this->db->like('RESPON_ETAX', 'AU010', 'after'); 
        $FL001  = $this->db->get('info_cash_etax')->result_array();

        foreach($FL001 as $rs) {
            $data = array( 
                "IS_SEND" => 'N',
                "IS_FILES" => 'N'
            );
                                
            $this->db->where('RECEIPT_NO', $rs['RECEIPT_NO']);
            $this->db->update('info_cash_etax', $data);
        }


        $this->db->order_by('E_ID', 'ASC');
        $this->db->where('IS_FILES','P');
        $this->db->where('CREATED < DATE_ADD(NOW(), INTERVAL -2 HOUR)');
        $ER_PROCESS  = $this->db->get('info_cash_etax')->result_array();

        foreach($ER_PROCESS as $rs) {
            $data = array( 
                "IS_SEND" => 'N',
                "IS_FILES" => 'N',
                "CREATED" => date('Y-m-d H:i:s')
            );
                                
            $this->db->where('RECEIPT_NO', $rs['RECEIPT_NO']);
            $this->db->update('info_cash_etax', $data);
        }

        
    }

    public function PCashReceipt(){

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','N');
            $this->db->like('CREATED',date('Y-m-d'));
            $Order  = $this->db->get('info_cash_etax')->row();


            if($Order){

                $RECEIPT_NO = $Order->RECEIPT_NO;

                    if($RECEIPT_NO){

                        $dataUpdate = array(
                                "IS_FILES" => "P"
                       );
                                     
                        $this->db->where_in('RECEIPT_NO',$RECEIPT_NO);
                        $this->db->update('info_cash_etax',$dataUpdate);

                    }

                require_once('nusoap/nusoap.php');
                $client = new nusoap_client('http://www.lcit.com/ServiceEtax.asmx?wsdl', 'WSDL', false, false, false, false, 0, 300);

                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }

                $FOUND_DRAFT = $client->call('FOUND_DRAFT',array('RECEIPT_NO' => $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($FOUND_DRAFT as $rs) {
                    $jsonData = $rs;
                }

                $Draft_WS = json_decode($jsonData, true);

                foreach ($Draft_WS as $rs) {
                    $comp_email = $rs['ORDER_SUPPORT_NO'];
                }

                 $stream_options = array(
                    'https' => array(
                       'method'  => 'GET',
                    ),
                );
                     $context  = stream_context_create($stream_options);
                     $response = file_get_contents("https://eport.one-stop-logistics.com/Preadvise/FetchEtax/".$comp_email, null, $context);


                $ws_etax = json_decode($response, true);

                if($ws_etax){
                    foreach ($ws_etax as $rs) {
                        $rs_email = $rs['company_email'];
                    }
                }

                if($ws_etax){
                    $company_email = $rs_email;
                } else {
                    $company_email = '';
                }


                $C_F_CONTROL = $client->call('C_F_CONTROL',array('RECEIPT_NO' => $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_F_CONTROL as $rs) {
                    $jsonData = $rs;
                }

                $c_control = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_DOC_HEAD = $client->call('C_DOC_HEAD',array('RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_DOC_HEAD as $rs) {
                    $jsonData = $rs;
                }

                $c_header = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_BUYER_INFO = $client->call('C_BUYER_INFO',array('CUS_EMAIL' => $company_email, 'RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_BUYER_INFO as $rs) {
                    $jsonData = $rs;
                }

                $c_buyer = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_TRADE_LINE = $client->call('C_TRADE_LINE',array('RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);


                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_TRADE_LINE as $rs) {
                    $jsonData = $rs;
                }

                $c_trade = json_decode($jsonData, true);


                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_FOOTER_REMARK = $client->call('C_FOOTER_REMARK',array('RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_FOOTER_REMARK as $rs) {
                    $jsonData = $rs;
                }

                $c_remark = json_decode($jsonData, true);

               

                $C_FOOTER = $client->call('C_FOOTER',array('RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_FOOTER as $rs) {
                    $jsonData = $rs;
                }

                $c_footer = json_decode($jsonData, true);

                foreach ($c_remark as $rs_rmk) {

                    if($rs_rmk['LINE_NO'] == '1'){
                        $c_footer[0]['DOCUMENT_REMARK8'] = $rs_rmk['PAY_DATE'].'|'.$rs_rmk['PAY_BY_DS'].'|'.$rs_rmk['CHEQUE_NO'].'|'.$rs_rmk['CURRENCY'].' '.$rs_rmk['LINE_AMOUNT'].'|'.$rs_rmk['EXCHANGE_RATE_LOCAL'].'|'.$rs_rmk['LINE_AMOUNT'].'|'.$rs_rmk['SYS_INS_USER_NM'];
                    } else if($rs_rmk['LINE_NO'] == '2'){
                        $c_footer[0]['DOCUMENT_REMARK9'] = $rs_rmk['PAY_DATE'].'|'.$rs_rmk['PAY_BY_DS'].'|'.$rs_rmk['CHEQUE_NO'].'|'.$rs_rmk['CURRENCY'].' '.$rs_rmk['LINE_AMOUNT'].'|'.$rs_rmk['EXCHANGE_RATE_LOCAL'].'|'.$rs_rmk['LINE_AMOUNT'].'|'.$rs_rmk['SYS_INS_USER_NM'];
                    } else if($rs_rmk['LINE_NO'] == '3'){
                        $c_footer[0]['DOCUMENT_REMARK10'] = $rs_rmk['PAY_DATE'].'|'.$rs_rmk['PAY_BY_DS'].'|'.$rs_rmk['CHEQUE_NO'].'|'.$rs_rmk['CURRENCY'].' '.$rs_rmk['LINE_AMOUNT'].'|'.$rs_rmk['EXCHANGE_RATE_LOCAL'].'|'.$rs_rmk['LINE_AMOUNT'].'|'.$rs_rmk['SYS_INS_USER_NM'];
                    }


                }


      
                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_TRAILER = $client->call('C_TRAILER',array('RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_TRAILER as $rs) {
                    $jsonData = $rs;
                }

                $c_trailer = json_decode($jsonData, true);


                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $files_etax = array_merge($c_control, $c_header, $c_buyer, $c_trade, $c_footer, $c_trailer);


                $handle = fopen(FCPATH.'public/tranfiles/cash/'.$RECEIPT_NO.'.txt', 'w');

                foreach($files_etax as $line){
                    foreach ($line as &$column) {

                        $replce_txt = str_replace('"','',$column);
                        $column = '"' . $replce_txt . '"';

                    }
                    unset($column);

                    fputcsv($handle, $line);
                }

                    fclose($handle);

                //read the entire string
                $str=file_get_contents(FCPATH.'public/tranfiles/cash/'.$RECEIPT_NO.'.txt');

                //replace something in the file string - this is a VERY simple example
                $str=str_replace('"""', '"',$str);

                //write the entire string
                file_put_contents(FCPATH.'public/tranfiles/cash/'.$RECEIPT_NO.'.txt', $str);
                    
                    if($RECEIPT_NO){

                        $dataUpdate = array(
                                "DATA_FILES" => 'public/tranfiles/cash/'.$RECEIPT_NO.'.txt',
                                "IS_FILES" => "Y",
                                "FILES_TM" => date('Y-m-d H:i:s')
                       );
                                     
                        $this->db->where_in('RECEIPT_NO',$RECEIPT_NO);
                        $this->db->update('info_cash_etax',$dataUpdate);

                    }


                exit;

                /*echo json_encode($c_trade);
                return false;*/ 
            }

    }

    public function CashReceipt(){

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'DESC');
            $this->db->where('IS_FILES','N');
            $Order  = $this->db->get('info_cash_etax')->row();

            if($Order){

                $RECEIPT_NO = $Order->RECEIPT_NO;

                    if($RECEIPT_NO){

                        $dataUpdate = array(
                                "IS_FILES" => "P"
                       );
                                     
                        $this->db->where_in('RECEIPT_NO',$RECEIPT_NO);
                        $this->db->update('info_cash_etax',$dataUpdate);

                    }

                require_once('nusoap/nusoap.php');
                $client = new nusoap_client('http://www.lcit.com/ServiceEtax.asmx?wsdl', 'WSDL', false, false, false, false, 0, 300);

                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }

                $FOUND_DRAFT = $client->call('FOUND_DRAFT',array('RECEIPT_NO' => $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($FOUND_DRAFT as $rs) {
                    $jsonData = $rs;
                }

                $Draft_WS = json_decode($jsonData, true);

                foreach ($Draft_WS as $rs) {
                    $comp_email = $rs['ORDER_SUPPORT_NO'];
                }

                 $stream_options = array(
                    'https' => array(
                       'method'  => 'GET',
                    ),
                );
                     $context  = stream_context_create($stream_options);
                     $response = file_get_contents("https://eport.one-stop-logistics.com/Preadvise/FetchEtax/".$comp_email, null, $context);


                $ws_etax = json_decode($response, true);

                if($ws_etax){
                    foreach ($ws_etax as $rs) {
                        $rs_email = $rs['company_email'];
                    }
                }

                if($ws_etax){
                    $company_email = $rs_email;
                } else {
                    $company_email = '';
                }


                $C_F_CONTROL = $client->call('C_F_CONTROL',array('RECEIPT_NO' => $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_F_CONTROL as $rs) {
                    $jsonData = $rs;
                }

                $c_control = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_DOC_HEAD = $client->call('C_DOC_HEAD',array('RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_DOC_HEAD as $rs) {
                    $jsonData = $rs;
                }

                $c_header = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_BUYER_INFO = $client->call('C_BUYER_INFO',array('CUS_EMAIL' => $company_email, 'RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_BUYER_INFO as $rs) {
                    $jsonData = $rs;
                }

                $c_buyer = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_TRADE_LINE = $client->call('C_TRADE_LINE',array('RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);


                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_TRADE_LINE as $rs) {
                    $jsonData = $rs;
                }

                $c_trade = json_decode($jsonData, true);


                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_FOOTER_REMARK = $client->call('C_FOOTER_REMARK',array('RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_FOOTER_REMARK as $rs) {
                    $jsonData = $rs;
                }

                $c_remark = json_decode($jsonData, true);

               

                $C_FOOTER = $client->call('C_FOOTER',array('RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_FOOTER as $rs) {
                    $jsonData = $rs;
                }

                $c_footer = json_decode($jsonData, true);

                foreach ($c_remark as $rs_rmk) {

                    if($rs_rmk['LINE_NO'] == '1'){
                        $c_footer[0]['DOCUMENT_REMARK8'] = $rs_rmk['PAY_DATE'].'|'.$rs_rmk['PAY_BY_DS'].'|'.$rs_rmk['CHEQUE_NO'].'|'.$rs_rmk['CURRENCY'].' '.$rs_rmk['LINE_AMOUNT'].'|'.$rs_rmk['EXCHANGE_RATE_LOCAL'].'|'.$rs_rmk['LINE_AMOUNT'].'|'.$rs_rmk['SYS_INS_USER_NM'];
                    } else if($rs_rmk['LINE_NO'] == '2'){
                        $c_footer[0]['DOCUMENT_REMARK9'] = $rs_rmk['PAY_DATE'].'|'.$rs_rmk['PAY_BY_DS'].'|'.$rs_rmk['CHEQUE_NO'].'|'.$rs_rmk['CURRENCY'].' '.$rs_rmk['LINE_AMOUNT'].'|'.$rs_rmk['EXCHANGE_RATE_LOCAL'].'|'.$rs_rmk['LINE_AMOUNT'].'|'.$rs_rmk['SYS_INS_USER_NM'];
                    } else if($rs_rmk['LINE_NO'] == '3'){
                        $c_footer[0]['DOCUMENT_REMARK10'] = $rs_rmk['PAY_DATE'].'|'.$rs_rmk['PAY_BY_DS'].'|'.$rs_rmk['CHEQUE_NO'].'|'.$rs_rmk['CURRENCY'].' '.$rs_rmk['LINE_AMOUNT'].'|'.$rs_rmk['EXCHANGE_RATE_LOCAL'].'|'.$rs_rmk['LINE_AMOUNT'].'|'.$rs_rmk['SYS_INS_USER_NM'];
                    }


                }


      
                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_TRAILER = $client->call('C_TRAILER',array('RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_TRAILER as $rs) {
                    $jsonData = $rs;
                }

                $c_trailer = json_decode($jsonData, true);


                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $files_etax = array_merge($c_control, $c_header, $c_buyer, $c_trade, $c_footer, $c_trailer);


                $handle = fopen(FCPATH.'public/tranfiles/cash/'.$RECEIPT_NO.'.txt', 'w');

                foreach($files_etax as $line){
                    foreach ($line as &$column) {

                        $replce_txt = str_replace('"','',$column);
                        $column = '"' . $replce_txt . '"';

                    }
                    unset($column);

                    fputcsv($handle, $line);
                }

                    fclose($handle);

                //read the entire string
                $str=file_get_contents(FCPATH.'public/tranfiles/cash/'.$RECEIPT_NO.'.txt');

                //replace something in the file string - this is a VERY simple example
                $str=str_replace('"""', '"',$str);

                //write the entire string
                file_put_contents(FCPATH.'public/tranfiles/cash/'.$RECEIPT_NO.'.txt', $str);
                    
                    if($RECEIPT_NO){

                         $dataUpdate = array(
                                "DATA_FILES" => 'public/tranfiles/cash/'.$RECEIPT_NO.'.txt',
                                "IS_FILES" => "Y",
                                "FILES_TM" => date('Y-m-d H:i:s')
                       );
                                     
                        $this->db->where_in('RECEIPT_NO',$RECEIPT_NO);
                        $this->db->update('info_cash_etax',$dataUpdate);

                    }


                exit;

                /*echo json_encode($c_trade);
                return false;*/ 
            }

    }

    public function FetchCND(){

            require_once('nusoap/nusoap.php');
                $client = new nusoap_client('http://www.lcit.com/ServiceEtax.asmx?wsdl', 'WSDL');

                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }

                $orderReceipt = $client->call('FetchCNDDate',array('S_DATE' => date('d-M-Y'), 'E_DATE' => date('d-M-Y')), '', '', false, true);
                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($orderReceipt as $rs) {
                    $jsonData = $rs;
                }

                $de_orderReceipt = json_decode($jsonData, true);

                foreach($de_orderReceipt as $rs_order){

                    $this->db->where('RECEIPT_NO',$rs_order['INVOICE_AN']);
                    $Order  = $this->db->get('info_cdn_etax')->result_array();

                    if(count($Order) == 0){

                        $data = array(  
                            "RECEIPT_NO" => $rs_order['INVOICE_AN'],
                            "INV_NO" =>  $rs_order['INV_NO'],
                            "CREATED" => date('Y-m-d H:i:s')
                        );
                                    
                        $this->db->insert('info_cdn_etax', $data);

                    }
                    
                }


    }

    public function CNDReceipt(){

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','N');
            $Order  = $this->db->get('info_cdn_etax')->row();

            if($Order){

                $RECEIPT_NO = $Order->RECEIPT_NO;
                $INV_NO = $Order->INV_NO;
        
                require_once('nusoap/nusoap.php');
                $client = new nusoap_client('http://www.lcit.com/ServiceEtax.asmx?wsdl', 'WSDL');

                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }
                $C_F_CONTROL = $client->call('CND_F_CONTROL',array('RECEIPT_NO' => $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_F_CONTROL as $rs) {
                    $jsonData = $rs;
                }

                $c_control = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_DOC_HEAD = $client->call('CND_DOC_HEAD',array('RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_DOC_HEAD as $rs) {
                    $jsonData = $rs;
                }

                $c_header = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_BUYER_INFO = $client->call('CND_BUYER_INFO',array('RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_BUYER_INFO as $rs) {
                    $jsonData = $rs;
                }

                $c_buyer = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_TRADE_LINE = $client->call('CND_TRADE_LINE',array('RECEIPT_NO' =>  $INV_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_TRADE_LINE as $rs) {
                    $jsonData = $rs;
                }

                $c_trade = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_FOOTER = $client->call('CND_FOOTER',array('RECEIPT_NO' =>  $RECEIPT_NO, 'INV_NO' =>  $INV_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_FOOTER as $rs) {
                    $jsonData = $rs;
                }

                $c_footer = json_decode($jsonData, true);


                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_TRAILER = $client->call('CND_TRAILER',array('RECEIPT_NO' =>  $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_TRAILER as $rs) {
                    $jsonData = $rs;
                }

                $c_trailer = json_decode($jsonData, true);


                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $files_etax = array_merge($c_control, $c_header, $c_buyer, $c_trade, $c_footer, $c_trailer);


               $handle = fopen(FCPATH.'public/tranfiles/credit_note/'.$RECEIPT_NO.'.txt', 'w');

                foreach($files_etax as $line){
                    foreach ($line as &$column) {

                        $replce_txt = str_replace('"','',$column);
                        $column = '"' . $replce_txt . '"';

                    }
                    unset($column);

                    fputcsv($handle, $line);
                }

                    fclose($handle);

                //read the entire string
                $str=file_get_contents(FCPATH.'public/tranfiles/credit_note/'.$RECEIPT_NO.'.txt');

                //replace something in the file string - this is a VERY simple example
                $str=str_replace('"""', '"',$str);

                //write the entire string
                file_put_contents(FCPATH.'public/tranfiles/credit_note/'.$RECEIPT_NO.'.txt', $str);
                
                        
                       if($RECEIPT_NO){

                         $dataUpdate = array(
                                "DATA_FILES" => 'public/tranfiles/credit_note/'.$RECEIPT_NO.'.txt',
                                "IS_FILES" => "Y",
                                "FILES_TM" => date('Y-m-d H:i:s')
                       );
                                     
                        $this->db->where_in('RECEIPT_NO',$RECEIPT_NO);
                        $this->db->update('info_cdn_etax',$dataUpdate);

                       }


                exit;


            }


    }

    public function FetchCRD(){

            require_once('nusoap/nusoap.php');
                $client = new nusoap_client('http://www.lcit.com/ServiceEtax.asmx?wsdl', 'WSDL');

                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }
                
                $orderReceipt = $client->call('FetchCRDDate',array('S_DATE' => date('d-M-Y'), 'E_DATE' => date('d-M-Y')), '', '', false, true);
                
                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($orderReceipt as $rs) {
                    $jsonData = $rs;
                }

                $de_orderReceipt = json_decode($jsonData, true);

                foreach($de_orderReceipt as $rs_order){

                    $this->db->where('RECEIPT_NO',$rs_order['RECEIPT_NO']);
                    $this->db->where('PAY_RECEIPT_ID',$rs_order['PAY_RECEIPT_ID']);
                    $Order  = $this->db->get('info_crd_etax')->result_array();

                    if(count($Order) == 0){

                        $data = array(  
                            "RECEIPT_NO" => $rs_order['RECEIPT_NO'],
                            "PAY_RECEIPT_ID" =>  $rs_order['PAY_RECEIPT_ID'],
                            "CREATED" => date('Y-m-d H:i:s')
                        );
                                    
                        $this->db->insert('info_crd_etax', $data);

                    }
                    
                }


    }


    public function CRDReceipt(){

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','N');
            $Order  = $this->db->get('info_crd_etax')->row();

            if($Order){

                $RECEIPT_NO = $Order->RECEIPT_NO;
                $PAY_RECEIPT_ID = $Order->PAY_RECEIPT_ID;
        
                require_once('nusoap/nusoap.php');
                $client = new nusoap_client('http://www.lcit.com/ServiceEtax.asmx?wsdl', 'WSDL', false, false, false, false, 0, 300);

                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }
                $C_F_CONTROL = $client->call('CRD_F_CONTROL',array('RECEIPT_NO' => $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_F_CONTROL as $rs) {
                    $jsonData = $rs;
                }

                $c_control = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_DOC_HEAD = $client->call('CRD_DOC_HEAD',array('RECEIPT_NO' => $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_DOC_HEAD as $rs) {
                    $jsonData = $rs;
                }

                $c_header = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_BUYER_INFO = $client->call('CRD_BUYER_INFO',array('PAY_RECEIPT_ID' => $PAY_RECEIPT_ID), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_BUYER_INFO as $rs) {
                    $jsonData = $rs;
                }

                $c_buyer = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_TRADE_LINE = $client->call('CRD_TRADE_LINE',array('PAY_RECEIPT_ID' => $PAY_RECEIPT_ID), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_TRADE_LINE as $rs) {
                    $jsonData = $rs;
                }

                $c_trade = json_decode($jsonData, true);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_FOOTER = $client->call('CRD_FOOTER',array('PAY_RECEIPT_ID' => $PAY_RECEIPT_ID), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_FOOTER as $rs) {
                    $jsonData_Footer = $rs;
                }

                $c_footer = json_decode($jsonData_Footer, true);

                ///////SEARCH AND REPLACE IN ARRAY WITH AMOUNT TO WORDS

                $CRD_AMOUNT_WORDS = $client->call('CRD_AMOUNT_WORDS',array('PAY_RECEIPT_ID' => $PAY_RECEIPT_ID), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($CRD_AMOUNT_WORDS as $rs) {
                    $jsonData = $rs;
                }

                $amount_in_words = json_decode($jsonData, true);

                foreach ($amount_in_words as $rs_am) {

                    $num = $rs_am['TOTAL_GROSS_AMOUNT'];

                }

                $array = json_decode(str_replace('AMOUNT_IN_WORDS',$this->AmountToWords($num), json_encode($jsonData_Footer)),true );

                $replace_c_footer = json_decode($array, true);
                ///////SEARCH AND REPLACE IN ARRAY WITH AMOUNT TO WORDS


                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $C_TRAILER = $client->call('CRD_TRAILER',array('RECEIPT_NO' => $RECEIPT_NO), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($C_TRAILER as $rs) {
                    $jsonData = $rs;
                }

                $c_trailer = json_decode($jsonData, true);


                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                $files_etax = array_merge($c_control,$c_header,$c_buyer,$c_trade,$replace_c_footer,$c_trailer);


                $handle = fopen(FCPATH.'public/tranfiles/credit/'.$RECEIPT_NO.'.txt', 'w');

                foreach($files_etax as $line){
                    foreach ($line as &$column) {

                        $replce_txt = str_replace('"','',$column);
                        $column = '"' . $replce_txt . '"';

                    }
                    unset($column);

                    fputcsv($handle, $line);
                }

                    fclose($handle);

                //read the entire string
                $str=file_get_contents(FCPATH.'public/tranfiles/credit/'.$RECEIPT_NO.'.txt');

                //replace something in the file string - this is a VERY simple example
                $str=str_replace('"""', '"',$str);

                //write the entire string
                file_put_contents(FCPATH.'public/tranfiles/credit/'.$RECEIPT_NO.'.txt', $str);

                        
                        if($RECEIPT_NO){

                            $dataUpdate = array(
                                    "DATA_FILES" => 'public/tranfiles/credit/'.$RECEIPT_NO.'.txt',
                                    "IS_FILES" => "Y",
                                    "FILES_TM" => date('Y-m-d H:i:s')
                           );
                                         
                            $this->db->where_in('RECEIPT_NO',$RECEIPT_NO);
                            $this->db->update('info_crd_etax',$dataUpdate);

                        }


                exit;


            }

    }

    public function LogEtax(){

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','N');
            $this->db->select('count(*) as c');
            $WaitToGenCredit  = $this->db->get('info_crd_etax')->row();

            echo 'Wait to Gen Credit : '.$WaitToGenCredit->c.'<br>';

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','Y');
            $this->db->where('IS_SEND','N');
            $this->db->select('count(*) as c');
            $WaitToSendCredit  = $this->db->get('info_crd_etax')->row();
            echo 'Wait to Send Credit : '.$WaitToSendCredit->c.'<br><br>';

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','N');
            $this->db->select('count(*) as c');
            $WaitToGenCDN  = $this->db->get('info_cdn_etax')->row();
            echo 'Wait to Gen Credit Note : '.$WaitToGenCDN->c.'<br>';

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','Y');
            $this->db->where('IS_SEND','N');
            $this->db->select('count(*) as c');
            $WaitToSendCDN  = $this->db->get('info_cdn_etax')->row();
            echo 'Wait to Send Credit Note : '.$WaitToSendCDN->c.'<br><br>';

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','N');
            $this->db->select('count(*) as c');
            $WaitToGenCash  = $this->db->get('info_cash_etax')->row();
            echo 'Wait to Gen Cash : '.$WaitToGenCash->c.'<br>';

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','P');
            $this->db->select('count(*) as c');
            $WaitToGenCash  = $this->db->get('info_cash_etax')->row();
            echo 'In Process Gen Cash : '.$WaitToGenCash->c.'<br>';

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','Y');
            $this->db->where('IS_SEND','N');
            $this->db->select('count(*) as c');
            $WaitToSendCash  = $this->db->get('info_cash_etax')->row();
            echo 'Wait to Send Cash : '.$WaitToSendCash->c.'<br><br>';


    }

    private function AmountToWords($amount = null){
       
       $num = $amount;
       $rest = substr(number_format(($amount), 2), -2);

        $num = str_replace(array(',', ' '), '' , trim($num));
            if(! $num) {
                return false;
            }
        $num = (int) $num;
        $words = array();
        $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
                                    'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
       );
        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
        $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
                                    'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
                                    'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
        );
        $num_length = strlen($num);
        $levels = (int) (($num_length + 2) / 3);
        $max_length = $levels * 3;
        $num = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);
            for ($i = 0; $i < count($num_levels); $i++) {
                $levels--;
                $hundreds = (int) ($num_levels[$i] / 100);
                $hundreds = ($hundreds ? '' . $list1[$hundreds] . ' hundred' . ' ' : '');
                $tens = (int) ($num_levels[$i] % 100);
                $singles = '';

                    if ( $tens < 20 ) {
                        $tens = ($tens ? '' . $list1[$tens] . ' ' : '' );
                    } else {
                        $tens = (int)($tens / 10);
                        $tens = '' . $list2[$tens] . ' ';
                        $singles = (int) ($num_levels[$i] % 10);
                        $singles = '' . $list1[$singles] . ' ';
                    }
               
               $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? '' . $list3[$levels] . ' ' : '' );
            } //end for loop
            
            $commas = count($words);
                if ($commas > 1) {
                    $commas = $commas - 1;
                }

            if($rest > 0){
                $set_amount =  implode('', $words)."and ".$rest."/100 THB";
            } else {
                $set_amount =  implode('', $words)." THB";
            }

            return  $set_amount;
    }   

   
}