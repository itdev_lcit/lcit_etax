<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class VerifyLogin extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
   $this->load->model('M_user','',TRUE);
 }
 
 function index()
 {

  if($this->session->userdata('logged_in')) { 
    
      redirect('Resend', 'refresh');

  } else {
    $this->load->library('form_validation');
   
     $this->form_validation->set_rules('username', 'Username', 'trim|required');
     $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');
   
     if($this->form_validation->run() == FALSE)
     {
       $this->load->view('login_view');
     }
     else
     {  

        $check_data = $this->session->userdata('logged_in');

        
        $user = $this->db->get_where('users', array('user_id' => $check_data['id']))->row();

        /*if(!empty($user->date_process)){
          if($user->date_process < date('Y-m-d H:i:s')){
            redirect('Logout', 'refresh');
          }
        }*/


        redirect('Resend', 'refresh');
       
     }
  }
 
 }
 
 function check_database($password)
 {
   $username = $this->input->post('username');
 
   $result = $this->M_user->login($username, $password);


   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username
       );
       $this->session->set_userdata('logged_in', $sess_array);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid username or password');
     return false;
   }
 }
}
?>