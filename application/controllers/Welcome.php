<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function view()
	{	
		$check_data = $this->session->userdata('logged_in');

		$this->view['header'] =  $this->load->view('mains/header','',true);
		$this->view['menu'] =  $this->load->view('mains/menu','',true);

		return $this->load->view('mains/index',$this->view);
	}
}
