<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require('Welcome.php');


class SendEtaxER extends Welcome {

    public function CashReceipt(){

        $this->db->limit(1);
        $this->db->order_by('E_ID', 'ASC');
        $this->db->where('IS_FILES','Y');
        $this->db->where('IS_SEND','ER');
        $Order  = $this->db->get('info_cash_etax')->row();

        if(!empty($Order)){
            $url = 'https://etaxsp.one.th/etaxdocumentws/etaxsigndocument';
            $txtfile = curl_file_create(realpath(FCPATH.$Order->DATA_FILES));
            
            $curl = curl_init();

            $headers = array(
              'Content-Type: multipart/form-data',
              'Authorization: MjY6cno2a2lKeGE2Q3JXOFNUcEYzcHlmZW0wN29LRDl3cUxKUTFjV3ZZOTpleUowWVhocFpDSTZJakF4TURVMU16a3dNRFl5TXpFaUxDSndZWE56ZDI5eVpDSTZJa3hqYVhSbGRHRjRNakVpZlFyNERhZ1M0OUxieWNXMkFQZU4ybUNnSmlwQklzaEFqRA=='
            );
            
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_POST => true,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLINFO_HEADER_OUT => true,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_POSTFIELDS => array(
                    'SellerTaxId'=>'0105539006231', 
                    'SellerBranchId'=>'00000', 
                    'APIKey'=>'AK2-3UY8R84Q6GFXD8QXZMFYNPUJRFOKWOT614C5GAAC88OVAGLD8F1HWAPB9LW05QQSACKVSN0FBI1H8WPO0NWU29MWO9854BBMW5OJ6IZOBUJFZRHV0ZPD4CP02PXLT95YHD6QX3TH01CZX7TJ7X4NBLKZLGQP8NF3BKZIU6NSW463VL61A8LBBAKIJSQS7M1TL2S50E6AGWRE85ZSK6AIHYDYXY7C19LKLFRFWXW3FAOAB61O0E5TGPBNC3P4X72BU',
                    'UserCode'=>'lcitetax',
                    'AccessKey'=>'LCITetax@21',
                    'ServiceCode' => 'S03',
                    'TextContent' => $txtfile 
                  ),
              ));

            $response = curl_exec($curl);
            $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $jsonArrayResponse = json_decode($response);


                    if($jsonArrayResponse->status == 'OK'){
                        $dataUpdate = array(
                            "RESPON_ETAX" => $response,
                            "IS_SEND" => "Y",
                            "SEND_TM" => date('Y-m-d H:i:s')
                        );
                                         
                        $this->db->where_in('RECEIPT_NO',$Order->RECEIPT_NO);
                        $this->db->update('info_cash_etax',$dataUpdate);
                    }  else {
                        
                        if($Order->RECEIPT_NO){

                            $dataUpdate = array(
                                "RESPON_ETAX" => $jsonArrayResponse->errorCode.' | '.$jsonArrayResponse->errorMessage,
                                "IS_SEND" => "ER",
                                "SEND_TM" => date('Y-m-d H:i:s')
                            );
                                             
                            $this->db->where_in('RECEIPT_NO',$Order->RECEIPT_NO);
                            $this->db->update('info_cash_etax',$dataUpdate);

                        }

                    }
                        
                   

            var_dump($response,$http_code);

            curl_close($curl);;
        }

    }

    public function CDNReceipt(){

        $this->db->limit(1);
        $this->db->order_by('E_ID', 'ASC');
        $this->db->where('IS_FILES','Y');
        $this->db->where('IS_SEND','ER');
        $Order  = $this->db->get('info_cdn_etax')->row();


        if(!empty($Order)){

            $url = 'https://etaxsp.one.th/etaxdocumentws/etaxsigndocument';
            $txtfile = curl_file_create(realpath(FCPATH.$Order->DATA_FILES));
            
            $curl = curl_init();

            $headers = array(
              'Content-Type: multipart/form-data',
              'Authorization: MjY6cno2a2lKeGE2Q3JXOFNUcEYzcHlmZW0wN29LRDl3cUxKUTFjV3ZZOTpleUowWVhocFpDSTZJakF4TURVMU16a3dNRFl5TXpFaUxDSndZWE56ZDI5eVpDSTZJa3hqYVhSbGRHRjRNakVpZlFyNERhZ1M0OUxieWNXMkFQZU4ybUNnSmlwQklzaEFqRA=='
            );
            
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_POST => true,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLINFO_HEADER_OUT => true,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_POSTFIELDS => array(
                    'SellerTaxId'=>'0105539006231', 
                    'SellerBranchId'=>'00000', 
                    'APIKey'=>'AK2-3UY8R84Q6GFXD8QXZMFYNPUJRFOKWOT614C5GAAC88OVAGLD8F1HWAPB9LW05QQSACKVSN0FBI1H8WPO0NWU29MWO9854BBMW5OJ6IZOBUJFZRHV0ZPD4CP02PXLT95YHD6QX3TH01CZX7TJ7X4NBLKZLGQP8NF3BKZIU6NSW463VL61A8LBBAKIJSQS7M1TL2S50E6AGWRE85ZSK6AIHYDYXY7C19LKLFRFWXW3FAOAB61O0E5TGPBNC3P4X72BU',
                    'UserCode'=>'lcitetax',
                    'AccessKey'=>'LCITetax@21',
                    'ServiceCode' => 'S03',
                    'TextContent' => $txtfile 
                  ),
              ));

            $response = curl_exec($curl);
            $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $jsonArrayResponse = json_decode($response);


                    if($jsonArrayResponse->status == 'OK'){
                        $dataUpdate = array(
                            "RESPON_ETAX" => $response,
                            "IS_SEND" => "Y",
                            "SEND_TM" => date('Y-m-d H:i:s')
                        );
                                         
                        $this->db->where_in('RECEIPT_NO',$Order->RECEIPT_NO);
                        $this->db->update('info_cdn_etax',$dataUpdate);
                    } else {

                        if($Order->RECEIPT_NO){

                            $dataUpdate = array(
                                "RESPON_ETAX" => $jsonArrayResponse->errorCode.' | '.$jsonArrayResponse->errorMessage,
                                "IS_SEND" => "ER",
                                "SEND_TM" => date('Y-m-d H:i:s')
                            );
                                             
                            $this->db->where_in('RECEIPT_NO',$Order->RECEIPT_NO);
                            $this->db->update('info_cdn_etax',$dataUpdate);
                        }
                   

                    }

            var_dump($response,$http_code);

            curl_close($curl);;

        }

    }

    public function CRNReceipt(){

        $this->db->limit(1);
        $this->db->order_by('E_ID', 'ASC');
        $this->db->where('IS_FILES','Y');
        $this->db->where('IS_SEND','ER');
        $Order  = $this->db->get('info_crd_etax')->row();


        if(!empty($Order)){

            $url = 'https://etaxsp.one.th/etaxdocumentws/etaxsigndocument';
            $txtfile = curl_file_create(realpath(FCPATH.$Order->DATA_FILES));
            
            $curl = curl_init();

            $headers = array(
              'Content-Type: multipart/form-data',
              'Authorization: MjY6cno2a2lKeGE2Q3JXOFNUcEYzcHlmZW0wN29LRDl3cUxKUTFjV3ZZOTpleUowWVhocFpDSTZJakF4TURVMU16a3dNRFl5TXpFaUxDSndZWE56ZDI5eVpDSTZJa3hqYVhSbGRHRjRNakVpZlFyNERhZ1M0OUxieWNXMkFQZU4ybUNnSmlwQklzaEFqRA=='
            );
            
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_POST => true,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLINFO_HEADER_OUT => true,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_POSTFIELDS => array(
                    'SellerTaxId'=>'0105539006231', 
                    'SellerBranchId'=>'00000', 
                    'APIKey'=>'AK2-3UY8R84Q6GFXD8QXZMFYNPUJRFOKWOT614C5GAAC88OVAGLD8F1HWAPB9LW05QQSACKVSN0FBI1H8WPO0NWU29MWO9854BBMW5OJ6IZOBUJFZRHV0ZPD4CP02PXLT95YHD6QX3TH01CZX7TJ7X4NBLKZLGQP8NF3BKZIU6NSW463VL61A8LBBAKIJSQS7M1TL2S50E6AGWRE85ZSK6AIHYDYXY7C19LKLFRFWXW3FAOAB61O0E5TGPBNC3P4X72BU',
                    'UserCode'=>'lcitetax',
                    'AccessKey'=>'LCITetax@21',
                    'ServiceCode' => 'S03',
                    'TextContent' => $txtfile 
                  ),
              ));

            $response = curl_exec($curl);
            $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $jsonArrayResponse = json_decode($response);


                    if($jsonArrayResponse->status == 'OK'){
                        $dataUpdate = array(
                            "RESPON_ETAX" => $response,
                            "IS_SEND" => "Y",
                            "SEND_TM" => date('Y-m-d H:i:s')
                        );
                                         
                        $this->db->where_in('RECEIPT_NO',$Order->RECEIPT_NO);
                        $this->db->update('info_crd_etax',$dataUpdate);
                    }  else {
                        
                        if($Order->RECEIPT_NO){

                            $dataUpdate = array(
                                "RESPON_ETAX" => $jsonArrayResponse->errorCode.' | '.$jsonArrayResponse->errorMessage,
                                "IS_SEND" => "ER",
                                "SEND_TM" => date('Y-m-d H:i:s')
                            );
                                             
                            $this->db->where_in('RECEIPT_NO',$Order->RECEIPT_NO);
                            $this->db->update('info_crd_etax',$dataUpdate);
                        }

                    }
                        
                   

            var_dump($response,$http_code);

            curl_close($curl);;

        }

    }
   
}