<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require('Welcome.php');


class Resend extends Welcome {

    public function Index(){
        if($this->session->userdata('logged_in')) {    

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','N');
            $this->db->select('count(*) as c');
            $data['WaitToGenCredit'] = $this->db->get('info_crd_etax')->row();

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','Y');
            $this->db->where('IS_SEND','N');
            $this->db->select('count(*) as c');
            $data['WaitToSendCredit'] = $this->db->get('info_crd_etax')->row();

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','N');
            $this->db->select('count(*) as c');
            $data['WaitToGenCDN'] = $this->db->get('info_cdn_etax')->row();

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','Y');
            $this->db->where('IS_SEND','N');
            $this->db->select('count(*) as c');
            $data['WaitToSendCDN'] = $this->db->get('info_cdn_etax')->row();

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','N');
            $this->db->select('count(*) as c');
            $data['wQGenCash'] = $this->db->get('info_cash_etax')->row();

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','P');
            $this->db->select('count(*) as c');
            $data['proGenCash'] = $this->db->get('info_cash_etax')->row();

            $this->db->limit(1);
            $this->db->order_by('E_ID', 'ASC');
            $this->db->where('IS_FILES','Y');
            $this->db->where('IS_SEND','N');
            $this->db->select('count(*) as c');
            $data['WaitToSendCash'] = $this->db->get('info_cash_etax')->row();

            $this->view['main'] =  $this->load->view('resend/index',$data,true);
            $this->view();
        } else {
            $this->load->helper(array('form'));
            $this->load->view('login_view');
        }

    }

    public function CAS(){
        if($this->session->userdata('logged_in')) {     

            $this->db->where_in('IS_SEND', ['ER','N']);
            $data['order']  = $this->db->get('info_cash_etax')->result_array();

            $this->view['main'] =  $this->load->view('resend/cash',$data,true);
            $this->view();

        } else {
            $this->load->helper(array('form'));
            $this->load->view('login_view');
        }

    }

    public function ReceiptCash(){

            $receipt = $this->input->post('receipt');

            require_once('nusoap/nusoap.php');
                $client = new nusoap_client('http://www.lcit.com/ServiceEtax.asmx?wsdl', 'WSDL');

                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }

                $orderReceipt = $client->call('FetchCashByNo',array('RECEIPT_NO' => $receipt), '', '', false, true);

                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($orderReceipt as $rs) {
                    $jsonData = $rs;
                }

                $de_orderReceipt = json_decode($jsonData, true);

                foreach($de_orderReceipt as $rs_order){

                    $this->db->where('RECEIPT_NO',$rs_order['RECEIPT_NO']);
                    $Order  = $this->db->get('info_cash_etax')->row();

                    if($Order== null){

                        $data = array(  
                            "RECEIPT_NO" => $rs_order['RECEIPT_NO'],
                            "CREATED" => date('Y-m-d H:i:s')
                        );
                                    
                        $this->db->insert('info_cash_etax', $data);

                    } else if ($Order->IS_SEND = 'ER'){

                        $data = array( 
                            "IS_SEND" => 'N',
                            "IS_FILES" => 'N'
                        );
                            
                        $this->db->where('RECEIPT_NO', $rs_order['RECEIPT_NO']);
                        $this->db->update('info_cash_etax', $data);

                    }
                }

                redirect('Resend/CAS', 'refresh');

    }

    
    public function CRD(){
        if($this->session->userdata('logged_in')) {     

            $this->db->where_in('IS_SEND', ['ER','N']);
            $data['order']  = $this->db->get('info_crd_etax')->result_array();

            $this->view['main'] =  $this->load->view('resend/crd',$data,true);
            $this->view();

        } else {
            $this->load->helper(array('form'));
            $this->load->view('login_view');
        }

    }

    public function ReceiptCrd(){

            $receipt = $this->input->post('receipt');

            require_once('nusoap/nusoap.php');
                $client = new nusoap_client('http://www.lcit.com/ServiceEtax.asmx?wsdl', 'WSDL');

                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }
                
                $orderReceipt = $client->call('FetchCrdByNo',array('RECEIPT_NO' => $receipt), '', '', false, true);
                
                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($orderReceipt as $rs) {
                    $jsonData = $rs;
                }

                $de_orderReceipt = json_decode($jsonData, true);

                foreach($de_orderReceipt as $rs_order){

                    $this->db->where('RECEIPT_NO',$rs_order['RECEIPT_NO']);
                    $this->db->where('PAY_RECEIPT_ID',$rs_order['PAY_RECEIPT_ID']);
                    $Order  = $this->db->get('info_crd_etax')->row();

                    if($Order== null){

                        $data = array(  
                            "RECEIPT_NO" => $rs_order['RECEIPT_NO'],
                            "PAY_RECEIPT_ID" =>  $rs_order['PAY_RECEIPT_ID'],
                            "CREATED" => date('Y-m-d H:i:s')
                        );
                                    
                        $this->db->insert('info_crd_etax', $data);

                    } else if ($Order->IS_SEND = 'ER'){

                        $data = array( 
                            "IS_SEND" => 'N',
                            "IS_FILES" => 'N'
                        );
                            
                        $this->db->where('RECEIPT_NO', $rs_order['RECEIPT_NO']);
                        $this->db->update('info_crd_etax', $data);

                    }
                    
                }

                redirect('Resend/CRD', 'refresh');

    }

    public function CDN(){
        if($this->session->userdata('logged_in')) {     

            $this->db->where_in('IS_SEND', ['ER','N']);
            $data['order']  = $this->db->get('info_cdn_etax')->result_array();

            $this->view['main'] =  $this->load->view('resend/cdn',$data,true);
            $this->view();

        } else {
            $this->load->helper(array('form'));
            $this->load->view('login_view');
        }

    }

    public function ReceiptCdn(){

            $receipt = $this->input->post('receipt');
            
            require_once('nusoap/nusoap.php');
                $client = new nusoap_client('http://www.lcit.com/ServiceEtax.asmx?wsdl', 'WSDL');

                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }
                
                $orderReceipt = $client->call('FetchCNDByNo',array('RECEIPT_NO' => $receipt), '', '', false, true);
                
                $error = $client->getError();
                if ($error) {
                    print_r($client->response);
                    print_r($client->getDebug());
                    die();
                }

                foreach ($orderReceipt as $rs) {
                    $jsonData = $rs;
                }

                $de_orderReceipt = json_decode($jsonData, true);

                foreach($de_orderReceipt as $rs_order){

                    $this->db->where('RECEIPT_NO',$rs_order['INVOICE_AN']);
                    $Order  = $this->db->get('info_cdn_etax')->row();

                    if($Order== null){

                        $data = array(  
                            "RECEIPT_NO" => $rs_order['INVOICE_AN'],
                            "INV_NO" =>  $rs_order['INV_NO'],
                            "CREATED" => date('Y-m-d H:i:s')
                        );
                                    
                        $this->db->insert('info_cdn_etax', $data);

                    } else if ($Order->IS_SEND = 'ER'){

                        $data = array( 
                            "IS_SEND" => 'N',
                            "IS_FILES" => 'N'
                        );
                            
                        $this->db->where('RECEIPT_NO', $rs_order['INVOICE_AN']);
                        $this->db->update('info_cdn_etax', $data);

                    }
                    
                }

                redirect('Resend/CDN', 'refresh');

    }
   
}