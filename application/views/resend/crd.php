<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Credit Receipt</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Credit Receipt (Maximum 50)
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="FrmVat" role="form" action="<?php echo site_url(); ?>Resend/ReceiptCrd" method="post" enctype="multipart/form-data">
                                         <div class="col-lg-12">    
                                            <div class="form-group">
                                              <label for="description">Receipt No.</label>
                                              <p id="validate-description" style="color: red;"></p>
                                              <textarea class="form-control" rows="5" id="receipt" name="receipt"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-12" align="right">    
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-outline btn-success saveType" id="saveSize">Send</button>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                                <div class="col-lg-12">
                                    
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Receipt</th>
                                            <th>Files TM</th>
                                            <th>Files</th>
                                            <th>Respon</th>
                                            <th>Send TM</th>
                                            <th>Send</th>
                                            <th>Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php 


                                        if($order){
                                            $i = 1;
                                            foreach ($order as $rs) { 
                                                
                                            ?>

                                                <tr class="r-type">
                                                    <td><a href="<?php echo base_url(); ?><?php echo $rs['DATA_FILES']; ?>" target="_blank"><?php echo $rs['RECEIPT_NO']; ?></a></td>
                                                    <td><?php if($rs['IS_FILES'] == 'Y') { echo date("Y-m-d H:i:s", strtotime($rs['FILES_TM'])); } else { echo '-'; } ?></td>
                                                    <td><?php echo $rs['IS_FILES']; ?></td>
                                                    <td><?php echo $rs['RESPON_ETAX']; ?></td>
                                                    <td><?php if($rs['IS_SEND'] == 'Y') { echo date("Y-m-d H:i:s", strtotime($rs['SEND_TM'])); } else { echo '-'; } ?></td>
                                                    <td><?php echo $rs['IS_SEND']; ?></td>
                                                    <td><?php echo date("Y-m-d H:i:s", strtotime($rs['CREATED'])); ?></td>
                                                </tr>

                                        <?php

                                           $i++; } 
                                        }else {
                                            echo '<tr align="center"><td colspan="7">-No Data-</td></tr>';
                                       }?>

                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">

</script>