<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Log Receipt</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Log
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>Receipt</th>
                                                <th>Type</th>
                                                <th>Files</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	<tr class="r-type">
                                                <td>Cash</td>
                                                <td>Waitting to Generate Files</td>
                                                <td><?php echo $wQGenCash->c; ?></td>
                                            </tr>
                                            <tr class="r-type">
                                                <td>Cash</td>
                                                <td>Processing Generate Files</td>
                                                <td><?php echo $proGenCash->c; ?></td>
                                            </tr>
                                            <tr class="r-type">
                                                <td>Cash</td>
                                                <td>Waitting to send Files</td>
                                                <td><?php echo $WaitToSendCash->c; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-lg-4">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>Receipt</th>
                                                <th>Type</th>
                                                <th>Files</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="r-type">
                                                <td>Credit</td>
                                                <td>Waitting to Generate Files</td>
                                                <td><?php echo $WaitToGenCredit->c; ?></td>
                                            </tr>
                                            <tr class="r-type">
                                                <td>Credit</td>
                                                <td>Waitting to send Files</td>
                                                <td><?php echo $WaitToSendCredit->c; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-lg-4">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>Receipt</th>
                                                <th>Type</th>
                                                <th>Files</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="r-type">
                                                <td>Credit Note</td>
                                                <td>Waitting to Generate Files</td>
                                                <td><?php echo $WaitToGenCDN->c; ?></td>
                                            </tr>
                                            <tr class="r-type">
                                                <td>Credit Note</td>
                                                <td>Processing Generate Files</td>
                                                <td><?php echo $WaitToSendCDN->c; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">

</script>